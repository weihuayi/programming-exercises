all: calculation_exercises.pdf   computer-basis.pdf   code-basis.pdf  \
   
calculation_exercises.pdf: calculation_exercises.tex
	xelatex -shell-escape calculation_exercises.tex
	xelatex -shell-escape calculation_exercises.tex
	evince calculation_exercises.pdf&

computer-basis.pdf: computer-basis.tex
	xelatex -shell-escape computer-basis.tex
	xelatex -shell-escape computer-basis.tex
	evince computer-basis.pdf&

code-basis.pdf: code-basis.tex
	xelatex -shell-escape code-basis.tex
	xelatex -shell-escape code-basis.tex
	evince code-basis.pdf&

.PHONY:clean  
clean:
	-rm -fr _minted* *.ps *.dvi *.aux *.toc *.idx *.ind *.ilg *.log *.out *~ *.tid *.tms *.pdf *.bak *.blg *.bbl *.gz *.snm *.nav _minted *.bcf *.xml 

